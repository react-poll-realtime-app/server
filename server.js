var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var pollController = require('./app/controllers/polls');
mongoose.connect('mongodb://localhost:27017/dbtest');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;
// var router = express.Router();
// var Bear = require('./app/models/bear');

var routes = require('./app/routes');

// router.use(function(req, res, next) {
//     // do logging
//     console.log('Something is happening.');
//     next(); // make sure we go to the next routes and don't stop here
// });

// router.route('/bears')
// .post((req, res) => {
//   var bear = new Bear();
//   bear.name = req.body.name;
//   bear.save((err, bear) => {
//     if (err){
//       return res.send(err);
//     }
//     console.log("BEARR", bear)
//
//     io.emit('add_bear', bear)
//     res.json({message: "Bear Created"})
//   })
// })
// .get((req, res) => {
//    Bear.find((err, bears) => {
//      if (err){
//        return res.send(err)
//      }
//
//      res.send(bears);
//    })
// })
//
// router.route('/bears/:bear_id')
// .get((req, res) => {
//   Bear.find({_id: req.params.bear_id}, (err, ans) => {
//     if (err){
//       return res.send(err);
//     }
//     res.send(ans)
//   })
// })

// router.get('/', (req, res) => {
//   res.json({answer: 'hello'})
// })
// app.get('/', function(req, res){
//   res.sendFile(__dirname + '/index.html');
// });

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use('/api', routes);
var clients = {};
//
io.on('connection', (socket) => {

  clients[socket.id] = socket;

  socket.on('enter', (data) => {
    socket.join(data.pollId);
    socket.broadcast.to(data.pollId).emit('userJoined', data.userName);
  })

  socket.on('subscribe_poll', (data) => {
    socket.join(data.pollId);
  })

  socket.on('add_option', (data) => {
    pollController.addOption(data.pollId, data.option, socket)
  })

  socket.on('start_poll', (data) => {
    pollController.startPoll(data.pollId, socket);
  })

  socket.on('poll_option_choose', (data) => {
    pollController.chooseOption(data.pollId, data.index, data.user, socket)
  });

  // socket.on('poll_status_change', (data) => {
  //   pollController.pollChangeStatus(data.pollId, data.status, socket);
  // })

  socket.on('desconnect', () => {
    delete clients[socket.id]
  })
})

// app.listen(port);
http.listen(port, () => {
  console.log("Listening on " + port);
})
console.log("MAGIC GONNA HAPPPEN");
