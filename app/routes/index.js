var express = require('express'),
    router = express.Router(),
    polls = require('./polls');

router.use('/polls', polls);
module.exports = router;
