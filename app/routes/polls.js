var express = require('express')
  , router = express.Router()
  , Polls = require('../controllers/polls');

router.get('/', (req, res) => {
  Polls.findAll((err, ans) => {
    if (err){
      return res.send(err)
    }

    res.send(ans);
  })
})

router.get('/:id', (req, res) => {
  Polls.findById(req.params.id, (err, ans) => {

    if (err){
      return res.send({error: err})
    }
    res.send(ans);
    // res.send(ans);
  })
})

router.post('/:id/join', (req, res) => {
  const {user} = req.body;
  const id = req.params.id;
  Polls.joinUser(id, user, (err, ans) => {
    if (err){
      return res.send(err)
    }
    res.send(ans);
  })
})
router.post('/', (req, res) => {
  console.log("REQ_BODY", req.body)
  const {description, user} = req.body;
  console.log("GOT_", description, user);
  Polls.create(description, user, (err, ans) => {
    if (err){
      return res.send({error: err})
    }
    res.send(ans);
  })
})

// router.post()

module.exports = router;
