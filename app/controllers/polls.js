var Poll = require('./../models/poll'),
    Counter = require('./../models/counter');

const getWinnerFromDuel = (arrDuel) => {
  if (arrDuel[0].voters.length > arrDuel[1].voters.length){
    return [arrDuel[0]]
  }
  else if (arrDuel[1].voters.length > arrDuel[0].voters.length){
    return [arrDuel[1]]
  }
  else{
    return arrDuel;
  }
}

const findAll = (cb) => {
  Poll.find(cb);
}
exports.findAll = findAll;

const findById = (id, cb) => {
  Poll.findOne({_id: id}, cb);
}
exports.findById = findById;

const addOption = (id, option, socket) => {
  Poll.update(
    {_id: id},
    {$push: {options: option}},
    (err, ans) => {
      if (err){
        return ;
      }

      // Send to the group + owner
      socket.broadcast.to(id).emit('optionAdded', option );
      socket.emit('optionAdded', option );
    }
  )
}
exports.addOption = addOption;

const startPoll = (id, socket) => {
  Poll.update(
    {_id: id},
    {status: "IN_GAME"},
    (err, ans) => {
      if (err){
        return;
      }

      getNextStep(id, socket);
    }
  )
}
exports.startPoll = startPoll;

const endPoll = (id, winner, socket) => {
  Poll.findOneAndUpdate(
    {_id: id},
    {status: "FINISHED", winner},
    {new: true},
    (err, newPoll) => {
      if (err){
        return;
      }
      socket.broadcast.to(id).emit('pollChanged', newPoll );
      socket.emit('pollChanged', newPoll );

    }
  )
}

const chooseOption = (id, index, user, socket) => {
  console.log("CHOOSE_OPTT", id, index, user);
  Poll.findOneAndUpdate(
    {_id: id, "inDuel.options.index": index},
    // {_id: id},
    {$addToSet: {"inDuel.options.$.voters": user}},
    {new: true},
    (err, newPoll) => {
      if (err){
        return;
      }

      socket.broadcast.to(id).emit('pollChanged', newPoll );
      socket.emit('pollChanged', newPoll );

      var votesSum = 0;
      Object.keys(newPoll.inDuel.options).map(k => {
        votesSum += newPoll.inDuel.options[k].voters.length;
      })

      if (votesSum >= newPoll.users.length){
        getNextStep(id, socket);
      }


    }
  )
}
exports.chooseOption = chooseOption;

const getNextStep = (id, socket) => {
    console.log("GONNA GET NEXT STEP");

  Poll.findOne({_id: id}, (err, ans) => {
    if (err){
      return;
    }
    const {options, users, inDuel} = ans;


    if (options.length > 2 || (inDuel.options.length >= 1 && options.length >= 1)){
      // var arrVoters = [];
      // users.forEach((user) => {
      //   arrVoters.push({user, vote: null});
      // });


      var allOptions = options;

      // Concat the winner (if there is/are)
      if (inDuel && inDuel.options && inDuel.options.length > 0 ){
        var winnerFromDuel = getWinnerFromDuel(inDuel.options);
        var arrNames = winnerFromDuel.map(opt => opt.name);
        allOptions = allOptions.concat(arrNames);
      }
      nextOptions = allOptions.slice(2, allOptions.length);
      var nextDuel = {totalVotes: 0,
                      options: [{name: allOptions[0], voters: [], index: 0},
                            {name: allOptions[1], voters: [], index: 1}
                      ]};

      Poll.findOneAndUpdate(
        {_id: id},
        {$set: {options: nextOptions, inDuel: nextDuel}},
        {new: true},
        (err, newDoc) => {
          console.log("HEYLOOK", err, newDoc);
          socket.broadcast.to(id).emit('pollChanged', newDoc );
          socket.emit('pollChanged', newDoc );
        }
      )
    }
    else{
      var winner = getWinnerFromDuel(inDuel.options);
      console.log("WINNER", winner);
      if (winner.length == 1){
        endPoll(id, winner[0].name, socket);
      }
    }
  })
}
exports.getNextStep = getNextStep;

const joinUser = (id, user, cb) => {
  Poll.findOneAndUpdate(
    {_id: id},
    {$push: {users: user}},
    {},
    cb
  )
}
exports.joinUser = joinUser;

const create = (description, userCreated, cb) => {
  var users = [userCreated];
  Counter.getNextSeq('poll', (err, id) => {
    if (err){
      return cb(err);
    }
      var poll = new Poll({
        _id: id,
        description,
        admin: userCreated,
        users,
        status: "CREATED"
      });
      poll.save(cb);
  })
}
exports.create = create;
