var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var counterSchema = new Schema({
  _id: String,
  seq: Number
});

var Counter = mongoose.model('Counter', counterSchema);


exports.getNextSeq = (name, cb) => {
  var ret = Counter.findOneAndUpdate(
    { _id: name},
    { $inc: {seq: 1 } },
    {upsert: true, returnNewDocument: true},
  (err, ans) => cb(err, ans && ans.seq));
}
