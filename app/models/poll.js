var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Counter = require('./counter');


// statuses:
/*
CREATED,
IN_GAME,
FINISHED
*/
var pollSchema = new Schema({
  _id: Number,
  description: String,
  options: [Schema.Types.Mixed],
  admin: String,
  status: String,
  users: [String],
  winner: String,
  inDuel: Schema.Types.Mixed
});

var Poll = mongoose.model('Poll', pollSchema);

module.exports = Poll;
